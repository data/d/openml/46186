# OpenML dataset: Car-Parts

https://www.openml.org/d/46186

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Monthly sales car parts. 2674 series. Jan 1998 - Mar 2002. 

Extracted from 'expsmooth' R package.

Preprocessing:

1 - Transposed the dataset.

2 - Renamed columns (originally id of car parts) to 'value_X' with X from 0 to 2673.

3 - Standardize the date to the format %Y-%m-%d.

4 - Created column 'id_series', with value 0, there is only one multivariate series.

5 - Ensured that there are no missing dates and that they are evenly spaced (monthly).

6 - Created column 'time_step' with increasing values of the time_step for the time series.

7 - Filled NaN values with 0. 

For some values, after some months, there are only NaNs, which could indicate that the car part was not sold anymore, thus 0 makes sense.

8 - Casted 'value_X' columns to int, and 'id_series' as 'category'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46186) of an [OpenML dataset](https://www.openml.org/d/46186). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46186/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46186/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46186/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

